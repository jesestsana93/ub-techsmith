Curso de udemy: "Universidad Bootstrap: crea un sitio web real [TechSmith]" por Erick Mines
Duración: 16:08 hrs

Herramientas:
Menu of canvas: menu que mueve todo el contenido y da paso a un menu que ha estado oculto
* https://www.cssscript.com/
* https://www.javascripting.com/
* https://mmenujs.com/ usado para el menu
* Mover código HTML en función a una serie de condiciones (video 57): https://github.com/codefog/juggler-js    
* Slide animado: https://swiperjs.com/ 
* Libreria para el encabezado fijo (video 104): https://github.com/somewebmedia/hc-sticky

Extensiones para visual studio code:
* Live Server 
* HTML Class Suggestions 
* SCSS Formatter
* Bookmarks